FROM archlinux:latest

COPY . /app

RUN pacman -Syu --noconfirm gcc

CMD g++ app/genesis.cpp -o mybinary &&\
    chmod +x app/test.sh &&\
    ./app/test.sh

