#!/bin/bash
echo "Starting testing."

OUTPUT=`./mybinary`
RETVAL=$?

if [ $RETVAL -eq 0 ]; then
  echo "Trying to execute binary: OK"
else
  echo "Trying to execute binary: FAIL"
  echo "Now exiting."
  exit 1
fi

echo "Testing if output of program is 'Hello World!'."
if [ "$OUTPUT" == "Hello world!" ]; then
  echo "Test passed."
else
  echo "Test failed."
  exit 1
fi
